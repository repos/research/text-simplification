# Text-Simplification

## Setup

In order to use the GPUs on the stat-boxes, I had to set up a specific virtual environment
```
virtualenv -p python3.9 venv
pip install torch==1.10.1+rocm4.2 torchvision==0.11.2+rocm4.2 torchaudio==0.10.1 -f https://download.pytorch.org/whl/rocm4.2/torch_stable.html  
pip install -r requirements.txt
```

You also need to install the [easse-package](https://github.com/feralvam/easse/) which is used for evaluating text simplification models:
```
git clone https://github.com/feralvam/easse.git
cd easse
pip install -e .
```