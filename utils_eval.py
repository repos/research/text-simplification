import numpy as np
import textstat
import language_tool_python
from summac.model_summac import SummaCZS
  
class SummaryEvaluator:
    def __init__(self, lang_code = "en"):
        self.lang = lang_code
        
    def score(self, texts_original, texts_model, granularity = "sentence", use_con = False):
        scores_simplicity = self.score_simplicity(texts_model)
        scores_simplicity_diff = self.score_simplicity_diff(texts_original, texts_model)
        scores_fluency = self.score_fluency(texts_model)
        scores_meaning_preservation = self.score_meaning_preservation(texts_original, texts_model, granularity=granularity, use_con=use_con)
        
        dict_result = {
            "simplicity" : {"fkgl_model": scores_simplicity, "fkgl_diff":scores_simplicity_diff},
            "fluency": {"nerrors_lt": scores_fluency},
            "meaning_preservation": {"summac": scores_meaning_preservation}
        }
        return dict_result
        
    def score_simplicity(self, texts):
        list_fkgl = []
        for text in texts:
            fkgl = np.round(textstat.flesch_kincaid_grade(text),2)
            list_fkgl+=[fkgl]
        return list_fkgl
    
    def score_simplicity_diff(self, texts_original, texts_model):
        fkgl_original = self.score_simplicity(texts_original)
        fkgl_model = self.score_simplicity(texts_model)
        score_diff = [ np.round(fkgl_model[i] - fkgl_original[i],2) for i in range(len(fkgl_original))]
        return score_diff
        
    def score_fluency(self, texts, remote_server=None):
        # set up LanguageTool
        if remote_server == None:
            tool = language_tool_python.LanguageTool(self.lang)
        else:
            # try the remote server otherwise fall back on local
            try:
                tool = language_tool_python.LanguageTool(self.lang, remote_server=remote_server)
            except:
                # in case of
                tool = language_tool_python.LanguageTool(self.lang)
        # run LanguageTool
        list_nerrors = []
        for text in texts:
            try:
                matches = list(tool.check(text))
            except:
                # in case the call throws an exception we return an empty list
                matches = []
            # TODO: add any filters if necessary
            nerrors = len(matches)
            list_nerrors += [nerrors]
        return list_nerrors
    
    def score_meaning_preservation(self, texts_original, texts_model, granularity = "sentence", use_con = False):
        model_summac = SummaCZS(granularity=granularity, model_name="vitc", device="cuda", use_con=use_con) # If you have a GPU: switch to: device="cuda"
        result = model_summac.score(texts_original, texts_model)
        scores = [np.round(h,2) for h in result["scores"]]
        return scores

        
        
    
    