{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evaluation metrics for summarization and simplification\n",
    "\n",
    "The aim is to easily calculate a set of interpretable metrics that will make it easier to evaluate the automatically generated simple summaries.\n",
    "While there are scores such as [BLEU](https://huggingface.co/spaces/evaluate-metric/bleu) or [SARI](https://huggingface.co/spaces/evaluate-metric/sari) these suffer from two main drawbacks:\n",
    "i) they require a reference output (i.e. a ground truth), which is often not available;\n",
    "ii) it is not clear what score indicates that the system is working well.\n",
    "\n",
    "Therefore, this metric captures three simpler but more interpretable metrics that capture 3 aspects that are often considered in human evaluation of simplifications (and summaries).\n",
    "\n",
    "\n",
    "\n",
    "# Which metrics\n",
    "\n",
    "### Simpicity: This metric should capture how easy it is to read the output (or how much easier than the original). For this, we can use readability scores:\n",
    "* [FKGL-score](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests#Flesch%E2%80%93Kincaid_grade_level) (or the difference in FKGL-score): FKGL approximates the number of years of education needed to understand the text\n",
    "* implemented with [textstat](https://github.com/textstat/textstat)\n",
    "* [multilingual readability score](https://aclanthology.org/2024.acl-long.342/) (MRS) could replace FKGL when applying to more languages\n",
    "\n",
    "### Fluency: This metric should capture that the output text is well-formed (e.g. correct grammar, spelling, etc)\n",
    "* [LanguageTool](https://languagetool.org/): count the number of grammar mistakes. \n",
    "* implemented with [language-tool-python](https://pypi.org/project/language-tool-python/)\n",
    "\n",
    "### Meaning preservation: This metric should capture that the information in the output text is factually consistent with the input.\n",
    "* [SummaC](https://github.com/tingofurro/summac): checks whether the output is consistent with the original text.\n",
    "implemented with the [pip-package](https://pypi.org/project/summac/)\n",
    "* Interpretation: the model calculates an entailment score P(entailment) which the probability that the model output is entailed by the original document.\n",
    "* the score is between 0 (no entailmentand/inconsistent) to 1 (high entailment/consistent)\n",
    "\n",
    "More details about model choice:\n",
    "* We use the SummaCZS model. The main advantage is that it is directly interpretable. It captures the probability that the model output is entailed by the original.  \n",
    "* We use \"vitc\" as a backbone model for NLI as that has been reported to yield the best results\n",
    "* We use \"use_con=False\", i.e. we consider only entailment. In the original paper, this was shown to yield best scores. \n",
    "Qualitative inspection showed that contradiction scores were not always reliable and/or easy to interpret.\n",
    "* We use \"granularity=paragraph-sentence\" (original: paragraph, summary: sentence). in the paper, it is recommended: summary should be sentence level, original on a coarser level.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys,os\n",
    "from utils_eval import SummaryEvaluator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "doc_original = \"\"\"Hypatia (born c. 350–370; died 415 AD) was a Neoplatonist philosopher, astronomer, and mathematician who lived in Alexandria, Egypt, then part of the Eastern Roman Empire. She was a prominent thinker in Alexandria where she taught philosophy and astronomy. Although preceded by Pandrosion, another Alexandrian female mathematician, she is the first female mathematician whose life is reasonably well recorded. Hypatia was renowned in her own lifetime as a great teacher and a wise counselor. She wrote a commentary on Diophantus's thirteen-volume Arithmetica, which may survive in part, having been interpolated into Diophantus's original text, and another commentary on Apollonius of Perga's treatise on conic sections, which has not survived. Many modern scholars also believe that Hypatia may have edited the surviving text of Ptolemy's Almagest, based on the title of her father Theon's commentary on Book III of the Almagest.\n",
    "Hypatia constructed astrolabes and hydrometers, but did not invent either of these, which were both in use long before she was born. She was tolerant toward Christians and taught many Christian students, including Synesius, the future bishop of Ptolemais. Ancient sources record that Hypatia was widely beloved by pagans and Christians alike and that she established great influence with the political elite in Alexandria. Toward the end of her life, Hypatia advised Orestes, the Roman prefect of Alexandria, who was in the midst of a political feud with Cyril, the bishop of Alexandria. Rumors spread accusing her of preventing Orestes from reconciling with Cyril and, in March 415 AD, she was murdered by a mob of Christians led by a lector named Peter.\n",
    "Hypatia's murder shocked the empire and transformed her into a \"martyr for philosophy\", leading future Neoplatonists such as the historian Damascius (c. 458 – c. 538) to become increasingly fervent in their opposition to Christianity. During the Middle Ages, Hypatia was co-opted as a symbol of Christian virtue and scholars believe she was part of the basis for the legend of Saint Catherine of Alexandria. During the Age of Enlightenment, she became a symbol of opposition to Catholicism. In the nineteenth century, European literature, especially Charles Kingsley's 1853 novel Hypatia, romanticized her as \"the last of the Hellenes\". In the twentieth century, Hypatia became seen as an icon for women's rights and a precursor to the feminist movement. Since the late twentieth century, some portrayals have associated Hypatia's death with the destruction of the Library of Alexandria, despite the historical fact that the library no longer existed during Hypatia's lifetime.\"\"\"\n",
    "\n",
    "doc_model = \"Hypatia of Alexandria was a prominent Neoplatonist philosopher, astronomer, and mathematician who lived in the Eastern Roman Empire during the 4th and 5th centuries AD. She was a renowned teacher and counselor, known for her work in mathematics and astronomy. Hypatia's life and death have been the subject of much interest, with her murder at the hands of a Christian mob in 415 AD shocking the empire and leading to her being revered as a martyr for philosophy. Her legacy has been co-opted by various groups over the centuries, including Christians, Enlightenment thinkers, and feminists.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "texts_original = [doc_original]\n",
    "texts_model = [doc_model]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run Evaluator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "evaluator = SummaryEvaluator()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'simplicity': {'fkgl_model': [12.6], 'fkgl_diff': [-0.3]}, 'fluency': {'nerrors_lt': [0]}, 'meaning_preservation': {'summac': [0.65]}}\n"
     ]
    }
   ],
   "source": [
    "result = evaluator.score(texts_original, texts_model)\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "venv_simplification",
   "language": "python",
   "name": "venv_simplification"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
